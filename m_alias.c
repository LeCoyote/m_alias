/*
 * Author: Yann Sionneau (fallen@epiknet.org)
 */

#include "unrealircd.h"

char *hostserv_alias = NULL;
char *nickserv_alias = NULL;
char *chanserv_alias = NULL;
char *memoserv_alias = NULL;
char *operserv_alias = NULL;
char *botserv_alias = NULL;
char *global_alias = NULL;

int alias_usermsg(Client *sptr, Client *to, MessageTag *mtags, char *text, SendType sendtype);

/* Place includes here */
#define MSG_PRIVATE     "PRIVMSG"       /* PRIV */

ModuleHeader MOD_HEADER
  = {
	"third/m_alias",	/* Name of module */
	"5.0", /* Version */
	"redirect privmsg to aliases", /* Short description of module */
	"EpikNet",
	"unrealircd-5"
    };

int alias_usermsg(Client *sptr, Client *to, MessageTag *mtags, char *text, SendType sendtype)
{
	Client *acptr;

	if (nickserv_alias && strcasecmp("nickserv", to->name) == 0)
	{
		acptr = find_person(nickserv_alias, NULL);
		if (acptr)
			sendto_one(acptr, NULL, ":%s PRIVMSG %s :%s", sptr->name, nickserv_alias, text);
		else
			sendto_one(sptr, NULL, err_str(ERR_NOSUCHNICK), me.name, sptr->name, nickserv_alias);
		return 0;
	}
	else if (chanserv_alias && strcasecmp("chanserv", to->name) == 0)
	{
		acptr = find_person(chanserv_alias, NULL);
		if (acptr)
			sendto_one(acptr, NULL, ":%s PRIVMSG %s :%s", sptr->name, chanserv_alias, text);
		else
			sendto_one(sptr, NULL, err_str(ERR_NOSUCHNICK), me.name, sptr->name, chanserv_alias);
		return 0;
	}
	else if (memoserv_alias && strcasecmp("memoserv", to->name) == 0)
	{
		acptr = find_person(memoserv_alias, NULL);
		if (acptr)
			sendto_one(acptr, NULL, ":%s PRIVMSG %s :%s", sptr->name, memoserv_alias, text);
		else
			sendto_one(sptr, NULL, err_str(ERR_NOSUCHNICK), me.name, sptr->name, memoserv_alias);
		return 0;
	}
	else if (botserv_alias && strcasecmp("botserv", to->name) == 0)
	{
		acptr = find_person(botserv_alias, NULL);
		if (acptr)
			sendto_one(acptr, NULL, ":%s PRIVMSG %s :%s", sptr->name, botserv_alias, text);
		else
			sendto_one(sptr, NULL, err_str(ERR_NOSUCHNICK), me.name, sptr->name, botserv_alias);
		return 0;
	}
	else if (operserv_alias && strcasecmp("operserv", to->name) == 0)
	{
		acptr = find_person(operserv_alias, NULL);
		if (acptr)
			sendto_one(acptr, NULL, ":%s PRIVMSG %s :%s", sptr->name, operserv_alias, text);
		else
			sendto_one(sptr, NULL, err_str(ERR_NOSUCHNICK), me.name, sptr->name, operserv_alias);
		return 0;
	}
	else if (hostserv_alias && strcasecmp("hostserv", to->name) == 0)
	{
		acptr = find_person(hostserv_alias, NULL);
		if (acptr)
			sendto_one(acptr, NULL, ":%s PRIVMSG %s :%s", sptr->name, hostserv_alias, text);
		else
			sendto_one(sptr, NULL, err_str(ERR_NOSUCHNICK), me.name, sptr->name, hostserv_alias);
		return 0;
	}
	else if (global_alias && strcasecmp("global", to->name) == 0)
	{
		acptr = find_person(global_alias, NULL);
		if (acptr)
			sendto_one(acptr, NULL, ":%s PRIVMSG %s :%s", sptr->name, global_alias, text);
		else
			sendto_one(sptr, NULL, err_str(ERR_NOSUCHNICK), me.name, sptr->name, global_alias);
		return 0;
	}

	return 1;
}

DLLFUNC int alias_config_test(ConfigFile *cf, ConfigEntry *ce, int type, int *errs)
{
	int errors = 0;
	ConfigEntry *cep;

	if (type != CONFIG_SET)
		return 0;

	if (!ce || !ce->ce_varname || strcmp(ce->ce_varname, "m_alias"))
		return 0;

	for (cep = ce->ce_entries; cep; cep = cep->ce_next)
	{
		if (!cep->ce_varname)
		{
			config_error("%s:%i: blank set::m_alias item",
				cep->ce_fileptr->cf_filename, cep->ce_varlinenum);
			errors++;
			continue;
		}
		if (!strcasecmp(cep->ce_varname, "nickserv_alias"))
		{
		}
		else if (!strcasecmp(cep->ce_varname, "chanserv_alias"))
		{
		}
		else if (!strcasecmp(cep->ce_varname, "operserv_alias"))
		{
		}
		else if (!strcasecmp(cep->ce_varname, "botserv_alias"))
		{
		}
		else if (!strcasecmp(cep->ce_varname, "hostserv_alias"))
		{
		}
		else if (!strcasecmp(cep->ce_varname, "global_alias"))
		{
		}
		else if (!strcasecmp(cep->ce_varname, "memoserv_alias"))
		{
		} else {
			config_error("%s:%i: unknown directive set::m_alias::%s",
				cep->ce_fileptr->cf_filename, cep->ce_varlinenum,
				cep->ce_varname);
	
			errors++;
		}
	}

	*errs = errors;
	return errors ? -1 : 1;
}

DLLFUNC int alias_config_run(ConfigFile *cf, ConfigEntry *ce, int type)
{
ConfigEntry *cep;

    /* We filter on CONFIG_SET here, which means we will only get set::something. */
    if (type != CONFIG_SET)
        return 0;

    /* We are only interrested in set::m_alias... */
    if (!ce || !ce->ce_varname || strcmp(ce->ce_varname, "m_alias"))
        return 0; // if it's anything else: we don't care

    for (cep = ce->ce_entries; cep; cep = cep->ce_next)
    {
        if (!strcmp(cep->ce_varname, "nickserv_alias"))
            safe_strdup(nickserv_alias, canonize(cep->ce_vardata));
        else if (!strcmp(cep->ce_varname, "chanserv_alias"))
            safe_strdup(chanserv_alias, canonize(cep->ce_vardata));
        else if (!strcmp(cep->ce_varname, "operserv_alias"))
            safe_strdup(operserv_alias, canonize(cep->ce_vardata));
        else if (!strcmp(cep->ce_varname, "botserv_alias"))
            safe_strdup(botserv_alias, canonize(cep->ce_vardata));
        else if (!strcmp(cep->ce_varname, "memoserv_alias"))
            safe_strdup(memoserv_alias, canonize(cep->ce_vardata));
        else if (!strcmp(cep->ce_varname, "hostserv_alias"))
            safe_strdup(hostserv_alias, canonize(cep->ce_vardata));
        else if (!strcmp(cep->ce_varname, "global_alias"))
            safe_strdup(global_alias, canonize(cep->ce_vardata));
    }

    return 1;
}

/* This is called on module init, before Server Ready */
MOD_INIT()
{
	HookAdd(modinfo->handle, HOOKTYPE_USERMSG, 0, alias_usermsg);
	HookAdd(modinfo->handle, HOOKTYPE_CONFIGRUN, 0, alias_config_run);
	return MOD_SUCCESS;
}

MOD_TEST()
{
	HookAdd(modinfo->handle, HOOKTYPE_CONFIGTEST, 0, alias_config_test);
	return MOD_SUCCESS;
}

/* Is first run when server is 100% ready */
MOD_LOAD()
{
	return MOD_SUCCESS;
}

/* Called when module is unloaded */
MOD_UNLOAD()
{
	safe_free(hostserv_alias);
	safe_free(operserv_alias);
	safe_free(chanserv_alias);
	safe_free(nickserv_alias);
	safe_free(global_alias);
	safe_free(memoserv_alias);
	safe_free(botserv_alias);
	return MOD_SUCCESS;
}
